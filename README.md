# Zabbix Docker Template
Файл конфигурации и скрипт обнаружение нужно положить в следующие папки
`docker.conf -> /etc/zabbix/zabbix_agentd.d/`
`docker_discover.sh -> /etc/zabbix/scripts/`

Выставляем права на скрипт, после чего добавляем пользователя zabbix в группу docker, и перезапускаем агента:
```
sudo chown zabbix:zabbix /etc/zabbix/scripts/docker_discover.sh
sudo chmod u+x /etc/zabbix/scripts/docker_discover.sh
sudo gpasswd --add zabbix docker
sudo service zabbix-agent restart
```

Также позволяем пользователю zabbix выполнять команды от имени администратора. С помощью команды visudo редактируем права. После строчки:

`   root ALL=(ALL:ALL) ALL`

добавляем строку:

`   zabbix ALL=(ALL) NOPASSWD:ALL`


1.  Импортировать шаблоны Docker host и Docker container - `zbx_docker_host.xml`, `zbx_docker_container.xml`
2.  К узлу, на котором установлена система контейниризации, необходимо добавить шаблон Docker host. Далее Zabbix сам обнаружит рабочие контейнеры и начнет собирать с них данные.